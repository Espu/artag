# ARTag

## Privacy Policy

At ARTag, we are committed to protecting the privacy and security of our users. This privacy policy explains how we collect, use, and safeguard personal information that is obtained through our mobile application.

### Information Collection and Use

ARTag requires access to the user's phone camera and location services to provide its augmented reality message functionality. 

The app does use third-party services that may collect information used to identify you.
Link to the privacy policy of third-party service providers used by the app
[Google Play Services](https://www.google.com/policies/privacy/)

We do not share any personal information with third parties without the user's explicit consent, unless required by law.

### Security

ARTag takes reasonable measures to safeguard the confidentiality and security of personal information. We use industry-standard encryption and authentication methods to protect user data during transmission and storage.

### Data Retention

ARTag retains user data for as long as necessary to provide the service, or as required by law. Users may delete their messages at any time, which will remove the associated location data from our database.

### Account Deletion and Created Messages Deletion

Users can delete their account or all their created messages in the ARTag application by navigaing to the menu and choosing "Delete account" or "Delete all messages".

### Updates

ARTag reserves the right to update this privacy policy at any time. Thus, you are advised to review this page periodically for any changes. I will notify you of any changes by posting the new Privacy Policy on this page.

This policy is effective as of 2023-04-26.

#### Contact

If you have any questions or concerns about this privacy policy, please contact us at virtualtag23@gmail.com.